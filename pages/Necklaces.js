import React from 'react';
import { client } from '../lib/client';
import { HeroBanner, Product } from '../components';

const Necklaces = ({ products, bannerData }) => {
  return (
    <div>
      <HeroBanner heroBanner={bannerData.length && bannerData[0]} />
      <div className="products-heading">
        <h2>Stylish Necklaces</h2>
        <p>Discover our beautiful collection of handcrafted necklaces that will elevate any look.</p>
      </div>
      <div className="products-container">
      {products
    .filter((item) => item.type === "necklace") // change "ring" to the desired type
    .map((item) => (
      <Product key={item._id} product={item} />
    ))}
      </div>
    </div>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "product"]';
  const products = await client.fetch(query);

  const bannerQuery = '*[_type == "banner"]';
  const bannerData = await client.fetch(bannerQuery);

  return {
    props: { products, bannerData },
  };
};

export default Necklaces;