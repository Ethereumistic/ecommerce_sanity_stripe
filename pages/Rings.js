import React from 'react';
import { client } from '../lib/client';
import { Product, RingBanner } from '../components';

const Rings = ({ products, bannerData }) => {
  return (
    <div>
      <RingBanner ringBanner={bannerData.length && bannerData[7]} />
      <div className="products-heading">
        <h2>Stylish Rings</h2>
        <p>Discover our beautiful collection of handcrafted rings that will elevate any look.</p>
      </div>
      <div className="products-container">
      {products
    .filter((item) => item.type === "ring") // change "ring" to the desired type
    .map((item) => (
      <Product key={item._id} product={item} />
    ))}
      </div>
    </div>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "product"]';
  const products = await client.fetch(query);

  const bannerQuery = '*[_type == "banner"]';
  const bannerData = await client.fetch(bannerQuery);

  return {
    props: { products, bannerData },
  };
};

export default Rings;