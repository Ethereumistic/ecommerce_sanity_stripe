import React from 'react';
import Link from 'next/link';
import { AiOutlineShopping } from 'react-icons/ai'

import { Cart } from './';
import { useStateContext} from '../context/StateContext';
import Menu from './Menu';

const Navbar = () => {
  const { showCart, setShowCart, totalQuantities } = useStateContext();


  return (
    <div className="navbar-container">
      <p className="logo">
        <Link href="/">Rod Silver Store</Link>
      </p>

      <div>
        <Menu />
        </div>

      <button type="button" className="cart-icon" onClick={() => setShowCart(true)}>
        <AiOutlineShopping />
        <span className="cart-item-qty">{totalQuantities}</span>
      </button>

      {showCart && <Cart />}
    </div>
  )
}

export default Navbar