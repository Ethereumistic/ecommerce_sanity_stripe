import React, { useState, useRef, useEffect } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Menu = () => {
  const [showDropdown, setShowDropdown] = useState(false);
  const dropdownRef = useRef(null);
  const router = useRouter();

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setShowDropdown(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dropdownRef]);

  const handleDropdownClick = (eventKey) => {
    setShowDropdown(false);
  };

  const handleDropdownToggle = () => {
    setShowDropdown(!showDropdown);
  };

  const handleMainButtonClick = () => {
    if (showDropdown) {
      setShowDropdown(false);
      router.push('/');
    } else {
      setShowDropdown(true);
    }
  };

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <div className="dropdown" ref={dropdownRef}>
            <button
              type="button"
              className="btn1 btn-secondary dropdown-toggle"
              onClick={handleMainButtonClick}
            >
              🌟 Jewelry 🌟
            </button>
            {showDropdown && (
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <Link href="/Rings">
                  <a className="dropdown-item" onClick={() => handleDropdownClick('💍 Rings 💍')}>
                    💍 Rings 💍
                  </a>
                </Link>
                <Link href="/Earrings">
                  <a className="dropdown-item" onClick={() => handleDropdownClick('💎 Earrings 💎')}>
                    💎 Earrings 💎
                  </a>
                </Link>
                <Link href="/Bracelets">
                  <a className="dropdown-item" onClick={() => handleDropdownClick('🌈 Bracelets 🧿')}>
                    🌈 Bracelets 🧿
                  </a>
                </Link>
                <Link href="/Necklaces">
                  <a className="dropdown-item" onClick={() => handleDropdownClick('📿 Necklaces 🔮')}>
                    📿 Necklaces 🔮
                  </a>
                </Link>
                <Link href="/Others">
                  <a className="dropdown-item" onClick={() => handleDropdownClick('🎁 Others 🎀')}>
                    🎁 Others 🎀
                  </a>
                </Link>
              </div>
            )}
          </div>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Menu;