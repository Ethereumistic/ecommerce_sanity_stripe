import React from 'react';
import Link from 'next/link';

import { urlFor } from '../lib/client';

const RingBanner = ({ ringBanner }) => {
  return (
    <div className="hero-banner-container">
      <div>
        <p className="beats-solo">{ringBanner.smallText}</p>
        <h3>{ringBanner.midText}</h3>
        <h1>{ringBanner.largeText1}</h1>
        <img src={urlFor(ringBanner.image)} alt="headphones" className="hero-banner-image" />

        <div>
          <Link href={`/product/${ringBanner.product}`}>
            <button type="button">{ringBanner.buttonText}</button>
          </Link>
          <div className="desc">
            <h5>Description</h5>
            <p>{ringBanner.desc}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RingBanner